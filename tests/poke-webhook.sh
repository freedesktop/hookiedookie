#!/bin/bash

SETTINGS="Settings.yaml"

if [[ ! -e "$SETTINGS" ]]; then
  echo "Unable to find $SETTINGS in $PWD"
  exit 1
fi

PORT=$(yq -r .port "$SETTINGS")
NS=$(yq -r ".webhooks[0].project.namespace" "$SETTINGS")
PROJECT=$(yq -r ".webhooks[0].project.project" "$SETTINGS")
TOKEN=$(yq -r ".webhooks[0].token" "$SETTINGS")

command="$1"
if [[ -z "$command" ]]; then
  echo "Usage: $(basename $0) <cmd>"
  echo "Where <cmd> is one of 'push-event', 'comment-event', etc."
  echo "See tests/*.json for the available events"
  exit 1
fi

json="tests/data/$command.json"

if [[ ! -e "$json" ]]; then
  echo "Unknown command $command or missing tests/$command.json"
  exit 1
fi

curl \
  localhost:$PORT/webhook/$NS/$PROJECT \
  -X POST -H "X-GitLab-Token: $TOKEN" \
  --json "$(cat "$json")"
