# hookiedookie

A GitLab webhook dispatcher service. This service listens on the configured
port (default: 8080) for the JSON data that is provided by
[GitLab webhooks](https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html).

It compares the incoming data against the configured list of projects and
conditions and where the conditions match, an external program is started.

For example, the project "foo/bar" may have a condition "if a new issue label is 'spam',
run /usr/bin/spamfighter <issuenumber>".

## why in Rust?

- first and foremost, rust is fun and it's a good excuse to start a project with
- rust has safety features which seems to be adequate for a web service that would run in the kubernetes cluster

## current design

The aim is to be configurable enough:
- all of the data should be in `Settings.yaml`
- we can instantiate more than one webhook by providing their config in `Settings.yaml`
- we can also add webhook-specific data in each configuration in `Settings.yaml`

We also need tests!

## Features:

### validate users

See https://gitlab.freedesktop.org/bentiss/hookiedookie/-/issues/2 for a blueprint.
The bot can automatically validate users who made the request but is hard enough for a basic simple processing to automatically get the approvals

### block helper

Gitlab currently only allows admins to take actions against a given user.

This works fine in a world without spambots, but quickly fall short when admins are not paid admins.

See https://gitlab.freedesktop.org/bentiss/hookiedookie/-/issues/3 for a blueprint.
