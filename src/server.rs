// SPDX-License-Identifier: MIT License
use crate::rule::SideEffect;
use crate::settings::{Settings, Webhook, Workload};
#[cfg(feature = "autoreload")]
use notify::Watcher;
use serde::Serialize;
use std::collections::HashMap;
use std::convert::From;
use std::process::Command;
use std::sync::Mutex;
use tempfile;

use actix_web::{
    dev::Service as _,
    guard::{self, Guard, GuardContext},
    web::{self, Data, ReqData},
    App, HttpMessage, HttpRequest, HttpResponse, HttpServer,
};

#[derive(Debug, Clone, Serialize)]
struct WorkloadError {
    pub message: String,
}

/// Error used in run_workload to make the flow simpler. This error
/// just contains a message since all we can do with it is print it anyway.
impl WorkloadError {
    fn from(msg: String) -> Self {
        Self { message: msg }
    }

    fn from_io_error(err: std::io::Error) -> Self {
        Self::from(format!("IO Error: {err}"))
    }

    fn from_static(msg: &'static str) -> Self {
        Self::from(String::from(msg))
    }
}

impl std::fmt::Display for WorkloadError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "WorkloadError: {}", self.message)
    }
}

impl From<std::io::Error> for WorkloadError {
    fn from(err: std::io::Error) -> Self {
        WorkloadError::from_io_error(err)
    }
}

impl From<std::string::FromUtf8Error> for WorkloadError {
    fn from(err: std::string::FromUtf8Error) -> Self {
        WorkloadError::from(format!("UTF8 conversion failed: {err}"))
    }
}

fn build_command(
    workload: Workload,
    cwd: std::path::PathBuf,
    env: HashMap<String, String>,
) -> Result<std::process::Command, WorkloadError> {
    // basic check that the command is actually provided
    if workload.run.is_empty() {
        return Err(WorkloadError::from_static(
            "invalid workload without run command",
        ));
    }

    let mut cmd = Command::new(workload.run[0].clone());

    cmd.args(workload.run.iter().skip(1));

    cmd.env_clear();
    cmd.env("PATH", std::env::var("PATH").unwrap());
    cmd.current_dir(cwd);
    cmd.envs(env);

    if let Some(envvars) = &workload.env {
        // First iterate over all of our input environment, and
        // pick up the ones we have in scope
        let filtered_env: HashMap<String, String> = std::env::vars()
            .filter(|&(ref k, _)| envvars.contains(k))
            .collect();

        cmd.envs(&filtered_env);

        for var in envvars {
            let parts: Vec<&str> = var.split("=").collect();
            if parts.len() > 1 {
                let key = parts[0];
                let value = parts[1..].join("=");
                cmd.env(key, value);
            }
        }
    }

    Ok(cmd)
}

enum WorkloadFlags {
    #[allow(unused)]
    WAIT,
    DONTWAIT,
}

fn run_workload(
    workload: Workload,
    env: HashMap<String, String>,
    flags: WorkloadFlags,
) -> Result<WorkloadResult, WorkloadError> {
    // Create a private tempdir to run commands in and pass that as TMPDIR.
    let tmp_prefix = "hookiedookie.";
    let tmp_dir = tempfile::Builder::new().prefix(&tmp_prefix).tempdir()?;
    let dirname = tmp_dir.path().to_owned();

    let mut cmd = build_command(workload.clone(), dirname, env)?;

    let result;

    if !workload.background {
        result = WorkloadResult::from(
            &workload,
            cmd.output().map_err(|e| WorkloadError::from_io_error(e)),
        );
    } else {
        let subprocess = cmd
            .stderr(std::process::Stdio::piped())
            .stdout(std::process::Stdio::piped())
            .spawn()?;

        result = WorkloadResult::new(&workload);
        let workload_copy = workload.clone();

        // keep the tempdir for the lifetime of the thread
        let tmp_dir_path = tmp_dir.into_path();

        let fn_thread = std::thread::spawn(move || {
            let res = WorkloadResult::from(
                &workload_copy,
                subprocess
                    .wait_with_output()
                    .map_err(|e| WorkloadError::from_io_error(e)),
            );

            println!("{}", serde_json::json!(res).to_string());
            std::fs::remove_dir_all(tmp_dir_path).expect("failed to clean up temporary directory");
        });

        if let WorkloadFlags::WAIT = flags {
            fn_thread.join().expect("failed to join process");
        }
    }
    println!("{}", serde_json::json!(result).to_string());
    Ok(result)
}

#[derive(Debug, Clone, Serialize)]
struct WorkloadResult {
    pub name: String,
    pub exit_code: Option<i32>,
    pub id: uuid::Uuid,
    pub error: Option<WorkloadError>,
    pub stderr: Option<String>,
    pub stdout: Option<String>,
}

impl WorkloadResult {
    fn new(workload: &Workload) -> Self {
        WorkloadResult {
            name: workload.name.clone(),
            id: workload.id.clone(),
            exit_code: None,
            error: None,
            stderr: None,
            stdout: None,
        }
    }

    fn from(workload: &Workload, result: Result<std::process::Output, WorkloadError>) -> Self {
        let mut wl_result = WorkloadResult::new(workload);

        match result {
            Ok(res) => {
                wl_result.stderr = Some(format!("{}", String::from_utf8_lossy(&res.stderr)));
                wl_result.stdout = Some(format!("{}", String::from_utf8_lossy(&res.stdout)));
                wl_result.exit_code = res.status.code();
            }
            Err(err) => wl_result.error = Some(err),
        }

        wl_result
    }
}

// Helper function to be able to test with a proper Webhook struct
async fn webhook_fn(_req: HttpRequest, config: Webhook, body: serde_json::Value) -> HttpResponse {
    // look for all matches
    let workloads: Vec<(Workload, SideEffect)> = config
        .workloads
        .iter()
        .filter_map(|w| w.rule.matches(&body).map(|se| (w.clone(), se)))
        .collect();

    // run all commands, each in a web::block
    let futures = workloads.iter().map(|(workload, side_effect)| {
        let mut wl_copy = workload.clone();
        wl_copy.generate_new_id();
        let wl_copy2 = wl_copy.clone();
        let se_copy = side_effect.clone();

        web::block(move || {
            run_workload(wl_copy, se_copy.env, WorkloadFlags::DONTWAIT)
                .or_else(|e: WorkloadError| {
                    Ok::<WorkloadResult, WorkloadError>(WorkloadResult::from(&wl_copy2, Err(e)))
                })
                .unwrap()
        })
    });

    let mut results: Vec<WorkloadResult> = Vec::new();

    // gather all results
    for result in futures::future::join_all(futures).await {
        match result {
            // If there was a blocking error, something wrong happened,
            // bail out.
            Err(e) => {
                return HttpResponse::InternalServerError()
                    .body(format!("Failed to run workload: {e}"))
            }
            Ok(r) => results.push(r),
        }
    }

    HttpResponse::Ok().body(serde_json::json!(results).to_string())
}

async fn webhook_fn_handler(
    req: HttpRequest,
    data: ReqData<Webhook>,
    body: web::Json<serde_json::Value>,
) -> HttpResponse {
    webhook_fn(req, data.into_inner(), body.into_inner()).await
}

struct ProjectWebhookFound;

impl Guard for ProjectWebhookFound {
    fn check(&self, req: &GuardContext) -> bool {
        // if there is no configuration for this webhook, abort
        req.req_data().get::<Webhook>().is_some()
    }
}

struct HealthzData {
    is_alive: bool,
}

impl HealthzData {
    fn new() -> Mutex<Self> {
        Mutex::new(Self { is_alive: false })
    }
}

async fn healthz_fn(data: web::Data<Mutex<HealthzData>>) -> HttpResponse {
    let data = data.lock().unwrap();
    if data.is_alive {
        HttpResponse::Ok().body("")
    } else {
        HttpResponse::InternalServerError().body("")
    }
}

fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        // bind to the url /webhook/any/path/is/possible/but/must/be/declared/in/the/config
        web::resource("/webhook/{full_path:.*}")
            // ensure the content type is JSON
            .guard(guard::Header("content-type", "application/json"))
            // add request data extracted from global settings
            .wrap_fn(|req, srv| {
                // we are before the request processing
                // ensure we have a gitlab token
                if let (Some(token), Some(settings)) = (
                    req.head().headers().get("x-gitlab-token"),
                    req.app_data::<Data<Mutex<Settings>>>(),
                ) {
                    let match_info = req.match_info();

                    if let Some(webhook) = settings.lock().unwrap().get(
                        // unwrap is fine because if it is not, then
                        // there is a bug in the Actix resource path handler
                        // and we better abort
                        match_info.get("full_path").unwrap(),
                        token.to_str().unwrap(),
                    ) {
                        req.extensions_mut().insert(webhook);
                    }
                }

                // future function for processing the response
                srv.call(req)
            })
            // now handle the post
            .route(
                web::post()
                    // ensure the token is valid and we found a webhook
                    // this needs to be here so we are sure the wrap_fn
                    // above is called before
                    .guard(ProjectWebhookFound)
                    // call the webhook function
                    .to(webhook_fn_handler),
            ),
    )
    .service(web::resource("/healthz").route(web::get().to(healthz_fn)));
}

#[cfg(feature = "autoreload")]
fn watch(
    settings_file: String,
    app_data: Data<Mutex<Settings>>,
    healthz: Data<Mutex<HealthzData>>,
) {
    // Create a channel to receive the events.
    let (tx, rx) = std::sync::mpsc::channel();

    // Automatically select the best implementation for your platform.
    // You can also access each implementation directly e.g. INotifyWatcher.
    let mut watcher: notify::RecommendedWatcher = notify::Watcher::new(
        tx,
        notify::Config::default().with_poll_interval(std::time::Duration::from_secs(10)),
    )
    .unwrap();

    // Add a path to be watched. All files and directories at that path and
    // below will be monitored for changes.
    watcher
        .watch(
            std::path::Path::new(settings_file.as_str()),
            notify::RecursiveMode::NonRecursive,
        )
        .unwrap();

    {
        healthz.lock().unwrap().is_alive = true;
    }

    // This is a simple loop, but we are in our own thread, so meh.
    loop {
        match rx.recv() {
            Ok(Ok(notify::Event {
                kind: notify::event::EventKind::Modify(_),
                ..
            })) => {
                healthz.lock().unwrap().is_alive = true;
                println!(
                    r#"{{"system": "{settings_file} written; refreshing configuration ..."}}"#
                );

                match Settings::from_file(settings_file.as_str()) {
                    Ok(new_settings) => {
                        app_data.lock().unwrap().copy_from(&new_settings);
                        println!(r#"{{"system": "{settings_file} successfully changed."}}"#);
                    }
                    Err(error) => eprintln!(
                        r#"{{"system": "error while reading {settings_file}: '{error}'"}}"#
                    ),
                }
            }

            Err(e) => {
                healthz.lock().unwrap().is_alive = false;
                eprintln!(r#"{{"system": "watch error: {e:?}"}}"#);
            }

            _ => {
                // Ignore event
            }
        }
    }
}

fn spawn_watcher(
    _settings_file: String,
    _settings_data: &web::Data<Mutex<Settings>>,
    _healthz_data: &web::Data<Mutex<HealthzData>>,
) -> () {
    cfg_if::cfg_if! {
        if #[cfg(feature = "autoreload")] {
            let settings_data_copy = Data::clone(_settings_data);
            let healthz_data_copy = Data::clone(_healthz_data);

            // We start the thread in detached mode.
            // It will be killed when the program terminates
            std::thread::spawn(move || {
                watch(_settings_file, settings_data_copy, healthz_data_copy);
            });
        } else {
            _healthz_data.lock().unwrap().is_alive = true
        }
    }
}

pub async fn run(settings_file: String) -> std::io::Result<()> {
    // unwrap may abort, but we are in the init phase
    let settings = Settings::from_file(settings_file.as_str()).unwrap();
    let listen_address = settings.listen_address.to_owned();
    let port = settings.port;
    let app_data = web::Data::new(Mutex::new(settings));
    let healthz_data = web::Data::new(HealthzData::new());

    spawn_watcher(settings_file.clone(), &app_data, &healthz_data);

    // Start the Actix web server
    HttpServer::new(move || {
        App::new()
            // append global settings to the web server
            // unwrap may abort the server, but we are in the init phase
            .app_data(Data::clone(&app_data))
            .app_data(Data::clone(&healthz_data))
            .configure(config)
    })
    .bind((listen_address, port))?
    .run()
    .await
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::rule::Operation;
    use crate::settings::Workload;
    use actix_service::Service;
    use actix_web::body::to_bytes;
    use actix_web::http::header::{HeaderName, HeaderValue, InvalidHeaderValue, TryIntoHeaderPair};
    use actix_web::web::Bytes;
    use actix_web::{http, test, web::Json};
    use serde::Serialize;
    use std::io::prelude::*;

    enum Expect {
        Output,
        NoOutput,
    }

    #[derive(Debug, Serialize, Clone)]
    #[allow(unused)]
    struct ExamplePayload {
        pub object_kind: String,
    }

    async fn webhook_test<'a>(payload: &'a str, workload: Workload) -> serde_json::Value {
        let payload = ExamplePayload {
            object_kind: String::from(payload),
        };
        let http_req = test::TestRequest::default()
            .set_json(payload.clone())
            .to_http_request();
        let svc_payload = test::TestRequest::default()
            .set_json(payload)
            .to_srv_request()
            .extract::<Json<serde_json::Value>>()
            .await
            .unwrap()
            .into_inner();
        let req_data = Webhook {
            path: String::from("testns/test_project"),
            token: String::from("test"),
            workloads: [workload].to_vec(),
        };
        let resp = webhook_fn(http_req, req_data, svc_payload).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let body = to_bytes(resp.into_body()).await.unwrap();

        serde_json::from_str(body.as_str()).unwrap()
    }

    #[actix_web::test]
    async fn push_ok() {
        let workload = Workload::new_with_rule(
            "push_rule",
            Box::new(["true"]),
            Operation::Equals {
                key: Some(String::from("object_kind")),
                value: String::from("push"),
            },
        );
        let initial_id = workload.id.clone();
        let body = webhook_test("push", workload).await;
        let body_array = body.as_array().unwrap();

        assert_eq!(body_array.len(), 1);

        let result = &body_array[0];
        assert!(result.get("error").unwrap().is_null());
        assert_eq!(result.get("stdout").unwrap().as_str(), Some(""));
        assert_eq!(result.get("stderr").unwrap().as_str(), Some(""));
        assert_eq!(result.get("name").unwrap().as_str(), Some("push_rule"));

        // a new uuid must have been generated for our request
        assert_ne!(
            result.get("id").unwrap().as_str(),
            Some(initial_id.to_string().as_str())
        );
    }

    #[actix_web::test]
    async fn push_nok() {
        let body = webhook_test(
            "puch",
            Workload::new_with_rule(
                "push_rule",
                Box::new(["test"]),
                Operation::Equals {
                    key: Some(String::from("object_kind")),
                    value: String::from("push"),
                },
            ),
        )
        .await;
        assert!(body.as_array().unwrap().is_empty());
    }

    #[test]
    async fn run_workload_ls_ok() {
        let output = run_workload(
            Workload::new("ls_call", Box::new(["ls"])),
            HashMap::new(),
            WorkloadFlags::DONTWAIT,
        )
        .expect("failed to wait on child");

        if let Some(err) = output.error {
            panic!("unexpected error: {:?}", err)
        }

        assert_eq!(
            output.stdout.unwrap(),
            "" // we're in a private tmpdir so no files exist
        );
    }

    #[test]
    async fn run_workload_echo_foo_ok() {
        let output = run_workload(
            Workload::new("echo foo", Box::new(["echo", "foo"])),
            HashMap::new(),
            WorkloadFlags::DONTWAIT,
        )
        .expect("failed to wait on child");

        if let Some(err) = output.error {
            panic!("unexpected error: {:?}", err)
        }

        assert_eq!(output.stdout.unwrap(), "foo\n");
    }

    #[test]
    async fn run_workload_background_echo_bar_ok() {
        let mut workload = Workload::new("echo bar", Box::new(["echo", "bar"]));
        workload.background = true;

        let output = run_workload(workload, HashMap::new(), WorkloadFlags::DONTWAIT)
            .expect("failed to wait on child");

        if let Some(err) = output.error {
            panic!("unexpected error: {:?}", err)
        }

        // stdout is None, not empty ("")
        assert_eq!(output.stdout, None);
    }

    #[test]
    async fn run_workload_background_touch_baz_ok() {
        let tmpdir = tempfile::TempDir::new().unwrap();
        let baz = tmpdir.path().join("baz");
        let mut workload = Workload::new(
            format!("touch {:?}", baz.to_str().unwrap()).as_str(),
            Box::new(["touch", baz.to_str().unwrap()]),
        );
        workload.background = true;

        let output = run_workload(workload, HashMap::new(), WorkloadFlags::WAIT)
            .expect("failed to wait on child");

        if let Some(err) = output.error {
            panic!("unexpected error: {:?}", err)
        }

        // stdout is None, not empty ("")
        assert_eq!(output.stdout, None);

        // function executed properly in background
        assert!(baz.exists());
    }

    #[test]
    async fn run_workload_ls_missing_nok() {
        let output = run_workload(
            Workload::new("executable is not found", Box::new(["does-not-exist"])),
            HashMap::new(),
            WorkloadFlags::DONTWAIT,
        )
        .expect("failed to wait on child");

        assert!(output
            .error
            .unwrap()
            .message
            .contains("No such file or directory"));
    }

    #[test]
    async fn run_workload_ls_file_nok() {
        let output = run_workload(
            Workload::new("ls invalid file", Box::new(["ls", "does-not-exist"])),
            HashMap::new(),
            WorkloadFlags::DONTWAIT,
        )
        .expect("failed to wait on child");

        if let Some(err) = output.error {
            panic!("unexpected error: {:?}", err)
        }

        assert_eq!(output.stdout.unwrap(), "");
    }

    #[test]
    async fn run_workload_empty_nok() {
        match run_workload(
            Workload::new("empty command", Box::new([])),
            HashMap::new(),
            WorkloadFlags::DONTWAIT,
        ) {
            Err(e) => assert_eq!(
                e.to_string(),
                "WorkloadError: invalid workload without run command"
            ),
            ok => panic!("Unexpected valid result: {ok:?}"),
        }
    }

    struct GitlabTokenHeader {
        pub token: String,
    }

    impl GitlabTokenHeader {
        pub fn from_static(token: &'static str) -> GitlabTokenHeader {
            GitlabTokenHeader {
                token: String::from(token),
            }
        }
        pub fn default() -> GitlabTokenHeader {
            GitlabTokenHeader::from_static("CHANGE-ME")
        }
    }

    impl TryIntoHeaderPair for GitlabTokenHeader {
        type Error = InvalidHeaderValue;

        fn try_into_pair(self) -> Result<(HeaderName, HeaderValue), Self::Error> {
            Ok((
                HeaderName::from_static("x-gitlab-token"),
                HeaderValue::from_str(self.token.as_str()).unwrap(),
            ))
        }
    }

    fn default_settings() -> Settings {
        Settings::from_static(
            r#"
        debug: false
        listen_address: "::"
        port: 8080
        webhooks:
          - path: "testns/project1"
            token: "CHANGE-ME"
            workloads: []
        "#,
        )
        .unwrap()
    }

    fn default_settings_with_workloads(workloads: &str) -> Settings {
        // below snippet is indented so the workloads go in the right place
        // when they're defined below in the test methods at the function-level.
        let yaml = format!(
            r#"
    debug: false
    listen_address: "::"
    port: 8080
    webhooks:
      - path: "testns/project1"
        token: "CHANGE-ME"
    {}
"#,
            workloads
        );
        Settings::from_string(yaml).unwrap()
    }

    fn default_uri() -> &'static str {
        "/webhook/testns/project1"
    }

    #[allow(dead_code)]
    fn default_issue_payload() -> &'static str {
        include_str!("../tests/data/issue-event.json")
    }

    #[allow(dead_code)]
    fn default_push_payload() -> &'static str {
        include_str!("../tests/data/push-event.json")
    }

    #[allow(dead_code)]
    fn default_comment_payload() -> &'static str {
        include_str!("../tests/data/comment-event.json")
    }

    macro_rules! request_tests {
        ($($name:ident: $value:expr,)*) => {
            $(
                #[actix_web::test]
                async fn $name() {
                    let (uri, token, json, expected) = $value;
                    let settings = default_settings();
                    let app_data = web::Data::new(Mutex::new(settings));
                    let app = test::init_service(App::new().app_data(app_data.clone()).configure(config)).await;
                    let req = test::TestRequest::post()
                        .uri(uri)
                        .insert_header(token)
                        .insert_header(http::header::ContentType::json())
                        .set_payload(json)
                        .to_request();
                    let res = app.call(req).await.unwrap();

                    assert_eq!(res.status(), expected);
                }
            )*
        }
    }

    request_tests! {
        index_page_returns_404: (
            "/",
            GitlabTokenHeader::default(),
            default_push_payload(),
            http::StatusCode::NOT_FOUND
        ),
        wrong_url_returns_404: (
            "/does/not/exist",
            GitlabTokenHeader::default(),
            default_push_payload(),
            http::StatusCode::NOT_FOUND
        ),
        // correct URL but the project doesn't exist
        wrong_project_returns_405: (
            "/webhook/testns/notfound",
            GitlabTokenHeader::default(),
            default_push_payload(),
            http::StatusCode::METHOD_NOT_ALLOWED
        ),
        // correct URL but the token is invalid
        wrong_token_returns_405: (
            "/webhook/testns/notfound",
            GitlabTokenHeader::from_static("wrong token value"),
            default_push_payload(),
            http::StatusCode::METHOD_NOT_ALLOWED
        ),
        // We only need to test a few json errors. The json
        // parser itself hopefully works correctly, so all we need
        // to check for is that *our* handling of invalid json
        // works, but not what the invalid json actually looks like.
        empty_payload_returns_400: (
            default_uri(),
            GitlabTokenHeader::default(),
            "", // invalid json
            http::StatusCode::BAD_REQUEST
        ),
        invalid_json_returns_400: (
            default_uri(),
            GitlabTokenHeader::default(),
            "{", // json parser error
            http::StatusCode::BAD_REQUEST
        ),
        runaway_quote_returns_400: (
            default_uri(),
            GitlabTokenHeader::default(),
            "{ \"foo\": \"bar }", // json parser error
            http::StatusCode::BAD_REQUEST
        ),
        empty_json_returns_200: (
            default_uri(),
            GitlabTokenHeader::default(),
            "{}", // almost empty json -> it should not match anything
            http::StatusCode::OK
        ),
    }

    #[actix_web::test]
    async fn missing_token_returns_405() {
        let settings = default_settings();
        let app_data = web::Data::new(Mutex::new(settings));
        let app = test::init_service(App::new().app_data(app_data.clone()).configure(config)).await;

        // skipping the gitlab token header
        let req = test::TestRequest::post()
            .uri(default_uri())
            .insert_header(http::header::ContentType::json())
            .set_payload(default_push_payload())
            .to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::METHOD_NOT_ALLOWED);
    }

    #[actix_web::test]
    async fn wrong_content_type_returns_404() {
        let settings = default_settings();
        let app_data = web::Data::new(Mutex::new(settings));
        let app = test::init_service(App::new().app_data(app_data.clone()).configure(config)).await;

        // Not setting content-type: application/json here
        let req = test::TestRequest::post().uri(default_uri()).to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::NOT_FOUND);
    }

    #[actix_web::test]
    async fn get_returns_405() {
        let settings = default_settings();
        let app_data = web::Data::new(Mutex::new(settings));
        let app = test::init_service(App::new().app_data(app_data.clone()).configure(config)).await;

        let req = test::TestRequest::get() // GET request, not POST
            .uri(default_uri())
            .insert_header(GitlabTokenHeader::default())
            .insert_header(http::header::ContentType::json())
            .set_payload(default_push_payload())
            .to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::METHOD_NOT_ALLOWED);
    }

    #[actix_web::test]
    async fn invalid_run_returns_200() {
        let rules = r#"
         - rule:
             equals:
               key: "object_kind"
               value: "push"
        "#;
        let workloads = format!(
            r#"
        workloads: {rules}
           run: []
           name: empty
        "#,
        );
        let settings = default_settings_with_workloads(workloads.as_str());
        println!("{settings:?}");
        let app_data = web::Data::new(Mutex::new(settings));
        let app = test::init_service(App::new().app_data(app_data.clone()).configure(config)).await;

        let req = test::TestRequest::post()
            .uri(default_uri())
            .insert_header(GitlabTokenHeader::default())
            .insert_header(http::header::ContentType::json())
            .set_payload(default_push_payload())
            .to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::OK);
        let body = to_bytes(res.into_body()).await.unwrap();

        assert!(body
            .as_str()
            .contains("{\"error\":{\"message\":\"invalid workload without run command\"}"));
        assert!(body.as_str().contains("\"name\":\"empty\""));
        assert!(body.as_str().contains("\"stderr\":null"));
        assert!(body.as_str().contains("\"stdout\":null"));
    }

    #[actix_web::test]
    async fn multiple_hooks_is_ok() {
        let yaml = r#"
    debug: false
    listen_address: "::"
    port: 8080
    webhooks:
      - path: "testns/project1"
        token: "FIRST-TOKEN"
        workloads:
         - rule:
             equals:
               key: "object_kind"
               value: "push"
           run: ["ls"]
           name: ls
      - path: "testns/project1"
        token: "SECOND-TOKEN"
        workloads:
         - rule:
             equals:
               key: "object_kind"
               value: "push"
           run: ["ls"]
           name: ls 2
        "#;
        let settings = Settings::from_string(String::from(yaml)).unwrap();
        println!("{settings:?}");
        let app_data = web::Data::new(Mutex::new(settings));
        let app = test::init_service(App::new().app_data(app_data.clone()).configure(config)).await;

        let req = test::TestRequest::post()
            .uri(default_uri())
            .insert_header(GitlabTokenHeader::from_static("FIRST-TOKEN"))
            .insert_header(http::header::ContentType::json())
            .set_payload(default_push_payload())
            .to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::OK);

        let req = test::TestRequest::post()
            .uri(default_uri())
            .insert_header(GitlabTokenHeader::from_static("SECOND-TOKEN"))
            .insert_header(http::header::ContentType::json())
            .set_payload(default_push_payload())
            .to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::OK);
    }

    trait BodyTest {
        fn as_str(&self) -> &str;
    }

    impl BodyTest for Bytes {
        fn as_str(&self) -> &str {
            std::str::from_utf8(self).unwrap()
        }
    }

    async fn workload_with_args(rules: &str, args: &str, expect_output: Expect) {
        let cwd_env = std::env::current_dir().unwrap();
        let cwd = cwd_env.to_str().unwrap();
        let workloads = format!(
            r#"
        workloads: {rules}
           run: ["bash", "-c", "{cwd}/tests/echo-helper.sh $EXPORTED"]
           name: echo helper
        "#,
        );
        let settings = default_settings_with_workloads(workloads.as_str());
        println!("{settings:?}");
        let app_data = web::Data::new(Mutex::new(settings));
        let app = test::init_service(App::new().app_data(app_data.clone()).configure(config)).await;

        // The server receives the payload and deserializes/reserializes it. This
        // reorders keys alphabetically and strips all whitespace, so this string
        // here is how it'll look like after this happens.
        let payload = "{\"object_kind\":\"push\",\"xyz\":{\"sub\":\"val\"},\"zar\":\"bar\",\"zoo\":1,\"zzz\":[\"bat\",\"bar\",\"baz\"]}";
        let req = test::TestRequest::post()
            .uri(default_uri())
            .insert_header(GitlabTokenHeader::default())
            .insert_header(http::header::ContentType::json())
            .set_payload(payload)
            .to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::OK);

        let body = to_bytes(res.into_body()).await.unwrap();

        match expect_output {
            Expect::NoOutput => assert_eq!("[]", body),
            Expect::Output => {
                assert!(!String::from(body.as_str()).contains("Failed to find executable"));

                let body_output = format!("{:}", body.as_str());

                let escaped_payload = payload.escape_default();

                println!("\nbody: {}\n", &body_output);

                assert!(body_output.contains(
                    format!(
                        "echo-helper.sh called with arguments: {}\\nPayload: {}",
                        args, escaped_payload
                    )
                    .as_str()
                ));
            }
        }
    }

    async fn workload_with_payload(rules: &str, expect_output: Expect) {
        workload_with_args(rules, "", expect_output).await
    }

    #[actix_web::test]
    async fn multiple_rules_is_ok() {
        let cwd_env = std::env::current_dir().unwrap();
        let cwd = cwd_env.to_str().unwrap();
        let yaml = format!(
            r#"
    debug: false
    listen_address: "::"
    port: 8080
    webhooks:
      - path: "testns/project1"
        token: "SUPER-SECRET_TOKEN"
        workloads:
         - rule:
             equals:
               key: "object_kind"
               value: "push"
           run: ["echo", "foo"]
           name: echo foo
         - rule:
             export:
               name: HOOKIE_PAYLOAD
           run: ["{cwd}/tests/echo-helper.sh"]
           name: echo helper
        "#
        );
        let settings = Settings::from_string(String::from(yaml)).unwrap();
        println!("{settings:?}");
        let app_data = web::Data::new(Mutex::new(settings));
        let app = test::init_service(App::new().app_data(app_data.clone()).configure(config)).await;
        let payload = "{\"object_kind\":\"push\",\"xyz\":{\"sub\":\"val\"},\"zar\":\"bar\",\"zoo\":1,\"zzz\":[\"bat\",\"bar\",\"baz\"]}";

        let req = test::TestRequest::post()
            .uri(default_uri())
            .insert_header(GitlabTokenHeader::from_static("SUPER-SECRET_TOKEN"))
            .insert_header(http::header::ContentType::json())
            .set_payload(payload)
            .to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::OK);

        let body = to_bytes(res.into_body()).await.unwrap();

        assert!(!String::from(body.as_str()).contains("Failed to find executable"));

        let body_output = format!("{:}", body.as_str());

        let escaped_payload = payload.escape_default();

        println!("\nbody: {}\n", &body_output);

        assert!(body_output.contains(r#""name":"echo foo""#));
        assert!(body_output.contains(
            format!(
                "echo-helper.sh called with arguments: \\nPayload: {}",
                escaped_payload
            )
            .as_str()
        ));
    }

    #[actix_web::test]
    async fn echo_workload_test_true_ok() {
        let rules = r#"
         - rule:
             and:
               - export:
                   name: HOOKIE_PAYLOAD
               - "true"
        "#;
        workload_with_payload(rules, Expect::Output).await
    }

    #[actix_web::test]
    async fn echo_workload_test_false_nok() {
        let rules = r#"
         - rule: "false"
        "#;
        workload_with_payload(rules, Expect::NoOutput).await
    }

    #[actix_web::test]
    async fn echo_workload_test_not_false_ok() {
        let rules = r#"
         - rule:
             and:
               - export:
                   name: HOOKIE_PAYLOAD
               - not: "false"
        "#;
        workload_with_payload(rules, Expect::Output).await
    }

    #[actix_web::test]
    async fn echo_workload_test_equals_ok() {
        let rules = r#"
         - rule:
             and:
               - export:
                   name: HOOKIE_PAYLOAD
               - equals:
                   key: "object_kind"
                   value: "push"
        "#;
        workload_with_payload(rules, Expect::Output).await
    }

    #[actix_web::test]
    async fn echo_workload_test_equals_nok() {
        // match on the wrong thing
        let rules = r#"
         - rule:
             equals:
               key: "object_kind"
               value: "commit"
        "#;
        workload_with_payload(rules, Expect::NoOutput).await
    }

    #[actix_web::test]
    async fn echo_workload_test_not_equals_ok() {
        let rules = r#"
         - rule:
             and:
               - export:
                   name: HOOKIE_PAYLOAD
               - not:
                   equals:
                     key: "object_kind"
                     value: "commit"
        "#;
        workload_with_payload(rules, Expect::Output).await
    }

    #[actix_web::test]
    async fn echo_workload_test_not_equals_nok() {
        let rules = r#"
         - rule:
             not:
               equals:
                 key: "object_kind"
                 value: "push"
        "#;
        workload_with_payload(rules, Expect::NoOutput).await
    }

    #[actix_web::test]
    async fn echo_workload_test_and_equals_ok() {
        let rules = r#"
         - rule:
             and:
               - export:
                   name: HOOKIE_PAYLOAD
               - equals:
                   key: "object_kind"
                   value: "push"
               - equals:
                   key: "zar"
                   value: "bar"
               - export:
                   key: "object_kind"
                   name: "EXPORTED"
        "#;
        workload_with_args(rules, "push", Expect::Output).await
    }

    #[actix_web::test]
    async fn echo_workload_test_and_equals_nok() {
        let rules = r#"
         - rule:
             and:
               - equals:
                   key: "object_kind"
                   value: "push"
               - equals:
                   key: "zar"
                   value: 1
               - export:
                   key: "object_kind"
                   name: "EXPORTED"
        "#;
        workload_with_args(rules, "", Expect::NoOutput).await
    }

    #[actix_web::test]
    async fn echo_workload_test_or_equals_ok() {
        let rules = r#"
         - rule:
             and:
               - export:
                   name: HOOKIE_PAYLOAD
               - or:
                   - equals:
                       key: "object_kind"
                       value: "commit"
                   - equals:
                       key: "zar"
                       value: "bar"
        "#;
        workload_with_payload(rules, Expect::Output).await
    }

    #[actix_web::test]
    async fn echo_workload_test_or_equals_nok() {
        let rules = r#"
         - rule:
             or:
               - equals:
                   key: "object_kind"
                   value: "comit"
               - equals:
                   key: "zoo"
                   value: 1
        "#;
        workload_with_payload(rules, Expect::NoOutput).await
    }

    #[actix_web::test]
    async fn echo_workload_test_any_ok() {
        let rules = r#"
         - rule:
             and:
               - export:
                   name: HOOKIE_PAYLOAD
               - any:
                   key: "zzz"
                   op:
                     substring:
                       needle: "at"
        "#;
        workload_with_payload(rules, Expect::Output).await
    }

    #[actix_web::test]
    async fn echo_workload_test_any_nok() {
        let rules = r#"
         - rule:
             any:
               key: "zzz"
               op:
                 substring:
                   needle: "aaa"
        "#;
        workload_with_payload(rules, Expect::NoOutput).await
    }

    #[actix_web::test]
    async fn echo_workload_test_all_ok() {
        let rules = r#"
         - rule:
             and:
               - export:
                   name: HOOKIE_PAYLOAD
               - all:
                   key: "zzz"
                   op:
                     substring:
                       needle: "a"
        "#;
        workload_with_payload(rules, Expect::Output).await
    }

    #[actix_web::test]
    async fn echo_workload_test_all_nok() {
        let rules = r#"
         - rule:
             all:
               key: "zzz"
               op:
                 substring:
                   needle: "at"
        "#;
        workload_with_payload(rules, Expect::NoOutput).await
    }

    #[actix_web::test]
    async fn echo_workload_test_follow_ok() {
        let rules = r#"
         - rule:
             and:
               - export:
                   name: HOOKIE_PAYLOAD
               - follow:
                   key: "xyz"
                   op:
                     equals:
                       key: "sub"
                       value: "val"
        "#;
        workload_with_payload(rules, Expect::Output).await
    }

    #[actix_web::test]
    async fn echo_workload_test_follow_nok() {
        let rules = r#"
         - rule:
             follow:
               key: "xyz"
               op:
                 not:
                   equals:
                     key: "sub"
                     value: "val"
        "#;
        workload_with_payload(rules, Expect::NoOutput).await
    }

    #[actix_web::test]
    async fn echo_workload_test_export_ok() {
        let rules = r#"
         - rule:
             and:
               - export:
                   name: HOOKIE_PAYLOAD
               - export:
                   key: "object_kind"
                   name: "EXPORTED"
                   op:
                     equals:
                       key: "object_kind"
                       value: "push"
        "#;
        workload_with_args(rules, "push", Expect::Output).await
    }

    #[actix_web::test]
    async fn echo_workload_test_export_false_nok() {
        let rules = r#"
         - rule:
             export:
               key: "object_kind"
               name: "EXPORTED"
               when: "success"
               op: "false"
        "#;
        workload_with_args(rules, "", Expect::NoOutput).await
    }

    #[actix_web::test]
    async fn echo_workload_test_export_noop_ok() {
        let rules = r#"
         - rule:
             and:
               - export:
                   name: HOOKIE_PAYLOAD
               - export:
                   key: "object_kind"
                   name: "EXPORTED"
        "#;
        workload_with_args(rules, "push", Expect::Output).await
    }

    #[actix_web::test]
    async fn echo_workload_test_export_when_failure_ok() {
        let rules = r#"
         - rule:
             and:
               - export:
                   name: HOOKIE_PAYLOAD
               - not:
                   export:
                       key: "object_kind"
                       name: "EXPORTED"
                       when: "failure"
                       op: "false"
        "#;
        workload_with_args(rules, "push", Expect::Output).await
    }

    #[actix_web::test]
    async fn echo_workload_test_export_when_always_ok() {
        let rules = r#"
         - rule:
             and:
               - export:
                   name: HOOKIE_PAYLOAD
               - not:
                   export:
                       key: "object_kind"
                       name: "EXPORTED"
                       when: "always"
                       op: "false"
        "#;
        workload_with_args(rules, "push", Expect::Output).await
    }

    // just being lazy for the difficult type annotations to extract
    macro_rules! test_init_service {
        ($($value:expr), *) => {
            $(
                {
                    let app_data = $value;
                    test::init_service(App::new().app_data(app_data.settings.clone()).app_data(app_data.healthz.clone()).configure(config)).await
                }
            )*
        }
    }

    struct MyData {
        settings: Data<Mutex<Settings>>,
        healthz: Data<Mutex<HealthzData>>,
    }

    impl MyData {
        fn new<'a>(settings_filename: &'a str) -> Self {
            let settings = Settings::from_file(settings_filename).unwrap();
            println!("{settings:?}");

            Self {
                settings: web::Data::new(Mutex::new(settings)),
                healthz: web::Data::new(HealthzData::new()),
            }
        }
    }

    fn autoreload_workload<'a>(settings_filename: &'a str, spawn: bool) -> MyData {
        let mut settings_file = std::fs::File::create(&settings_filename).unwrap();
        settings_file
            .write_all(
                br#"
    debug: false
    listen_address: "::"
    port: 8080
    webhooks:
      - path: "testns/project1"
        token: "SUPER-SECRET_TOKEN"
        workloads:
         - rule:
             equals:
               key: "object_kind"
               value: "push"
           name: echo foo
           run: ["echo", "foo"]"#,
            )
            .unwrap();

        let data = MyData::new(settings_filename);

        if spawn {
            spawn_watcher(
                String::from(settings_filename),
                &data.settings,
                &data.healthz,
            );
        }

        data
    }

    #[cfg(feature = "autoreload")]
    #[actix_web::test]
    async fn autoreload_is_ok() {
        let tmpdir = tempfile::TempDir::new().unwrap();
        let settings_path = tmpdir.path().join("Settings.yaml");
        let settings_filename = settings_path.to_str().unwrap();

        let app_data = autoreload_workload(settings_filename, true);

        let app = test_init_service!(&app_data);
        let payload = "{\"object_kind\":\"push\",\"xyz\":{\"sub\":\"val\"},\"zar\":\"bar\",\"zoo\":1,\"zzz\":[\"bat\",\"bar\",\"baz\"]}";

        let req = test::TestRequest::post()
            .uri(default_uri())
            .insert_header(GitlabTokenHeader::from_static("SUPER-SECRET_TOKEN"))
            .insert_header(http::header::ContentType::json())
            .set_payload(payload)
            .to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::OK);

        let body = to_bytes(res.into_body()).await.unwrap();

        assert!(!String::from(body.as_str()).contains("Failed to find executable"));

        let body_output = format!("{:}", body.as_str());

        println!("\nbody: {}\n", &body_output);

        assert!(body_output.contains(r#""name":"echo foo""#));

        let mut settings_file = std::fs::File::create(&settings_filename).unwrap();
        settings_file
            .write_all(
                br#"
    debug: false
    listen_address: "::"
    port: 8080
    webhooks:
      - path: "testns/project1"
        token: "CHANGED-TOKEN"
        workloads:
         - rule:
             equals:
               key: "object_kind"
               value: "push"
           name: echo bar
           run: ["echo", "bar"]"#,
            )
            .unwrap();

        let end = std::time::Instant::now() + std::time::Duration::from_secs(30);

        while std::time::Instant::now() < end {
            // not ideal, but we need to give time for the settings to be reloaded
            std::thread::sleep(std::time::Duration::from_millis(10));

            if app_data.settings.lock().unwrap().webhooks[0].token
                != String::from("SUPER-SECRET_TOKEN")
            {
                break;
            }
        }

        let req = test::TestRequest::post()
            .uri(default_uri())
            .insert_header(GitlabTokenHeader::from_static("SUPER-SECRET_TOKEN"))
            .insert_header(http::header::ContentType::json())
            .set_payload(payload)
            .to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::METHOD_NOT_ALLOWED);
    }

    #[cfg(feature = "autoreload")]
    #[actix_web::test]
    async fn delete_settings_is_ok() {
        let tmpdir = tempfile::TempDir::new().unwrap();
        let settings_path = tmpdir.path().join("TransientSettings.yaml");
        let settings_filename = settings_path.to_str().unwrap();

        let app_data = autoreload_workload(settings_filename, true);

        let app = test_init_service!(app_data);
        let payload = "{\"object_kind\":\"push\",\"xyz\":{\"sub\":\"val\"},\"zar\":\"bar\",\"zoo\":1,\"zzz\":[\"bat\",\"bar\",\"baz\"]}";

        let req = test::TestRequest::post()
            .uri(default_uri())
            .insert_header(GitlabTokenHeader::from_static("SUPER-SECRET_TOKEN"))
            .insert_header(http::header::ContentType::json())
            .set_payload(payload)
            .to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::OK);

        let body = to_bytes(res.into_body()).await.unwrap();

        assert!(!String::from(body.as_str()).contains("Failed to find executable"));

        let body_output = format!("{:}", body.as_str());

        println!("\nbody: {}\n", &body_output);

        assert!(body_output.contains(r#""name":"echo foo""#));

        std::fs::remove_file(&settings_filename).unwrap();

        // not ideal, but we need to give time for the settings to be reloaded
        std::thread::sleep(std::time::Duration::from_millis(500));

        let req = test::TestRequest::post()
            .uri(default_uri())
            .insert_header(GitlabTokenHeader::from_static("SUPER-SECRET_TOKEN"))
            .insert_header(http::header::ContentType::json())
            .set_payload(payload)
            .to_request();
        let res = app.call(req).await.unwrap();

        // if the settings file is deleted, we keep the last known config
        assert_eq!(res.status(), http::StatusCode::OK);
    }

    #[actix_web::test]
    async fn healthz_is_ok() {
        let tmpdir = tempfile::TempDir::new().unwrap();
        let settings_path = tmpdir.path().join("TransientSettings.yaml");
        let settings_filename = settings_path.to_str().unwrap();

        let app_data = autoreload_workload(settings_filename, true);

        let app = test_init_service!(app_data);
        let req = test::TestRequest::get().uri("/healthz").to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::OK);
    }

    #[actix_web::test]
    async fn healthz_is_nok() {
        let tmpdir = tempfile::TempDir::new().unwrap();
        let settings_path = tmpdir.path().join("TransientSettings.yaml");
        let settings_filename = settings_path.to_str().unwrap();

        // let's not start the watch thread, which will change the healthz
        // is_alive to false
        let app_data = autoreload_workload(settings_filename, false);

        let app = test_init_service!(app_data);
        let req = test::TestRequest::get().uri("/healthz").to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::INTERNAL_SERVER_ERROR);
    }
}
