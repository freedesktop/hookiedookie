// SPDX-License-Identifier: MIT License
use serde_derive::Deserialize;
use std::collections::HashMap;

#[derive(Debug, Default, Deserialize, Clone)]
#[serde(rename_all = "lowercase")]
pub enum When {
    #[default]
    Success,
    Failure,
    Always,
}

/// A set of operations to apply to our data sets to identify which elements of the data set we
/// want to work on.
#[derive(Debug, Default, Deserialize, Clone)]
#[serde(rename_all = "lowercase")]
pub enum Operation {
    /// Always evaluates to true
    #[default]
    True,
    /// Always evaluates to false
    False,
    /// All nested operations must evaluate to True
    And(Vec<Operation>),
    /// Any nested operation must evaluate to True
    Or(Vec<Operation>),
    /// The nested operation must evaluate to False
    Not(Box<Operation>),
    /// True if the string value for the element named by key contains the substring needle
    Substring { key: Option<String>, needle: String },
    /// True if the string value for the element named by key is equivalent to value
    Equals { key: Option<String>, value: String },
    /// Apply the operation to the element at the given key. This is roughly equivalent of
    /// the . (dot) operator in Rust for a struct.
    Follow { key: String, op: Box<Operation> },
    /// Apply the operation to the list or dict element at the given key and evaluate
    /// to true if the operation evaluates to true for any element.
    Any { key: String, op: Box<Operation> },
    /// Apply the operation to the list or dict element at the given key and evaluate
    /// to true if the operation evaluates to true for all elements.
    All { key: String, op: Box<Operation> },
    /// Export to the named environment variable and return the return value (or true)
    Export {
        key: Option<String>,
        name: String,
        when: Option<When>,
        op: Option<Box<Operation>>,
    },
}

#[derive(Debug, Default, Clone)]
pub struct SideEffect {
    pub env: HashMap<String, String>,
}

impl Operation {
    fn apply(&self, data: &serde_json::Value, side_effect: &mut SideEffect) -> bool {
        match &self {
            Operation::Substring { key, needle } => {
                let v = match key {
                    Some(keyname) => &data[keyname],
                    None => &data,
                };
                match v {
                    serde_json::Value::Null => false,
                    _ => v.is_string() && v.as_str().unwrap().contains(needle),
                }
            }
            Operation::Equals { key, value } => {
                let v = match key {
                    Some(keyname) => &data[keyname],
                    None => &data,
                };
                match v {
                    serde_json::Value::Null => false,
                    _ => v.is_string() && v.as_str().unwrap() == value,
                }
            }
            Operation::Follow { key, op } => {
                let v = &data[key];
                match v {
                    serde_json::Value::Null => false,
                    d => op.apply(d, side_effect),
                }
            }
            Operation::Not(other) => !other.apply(data, side_effect),
            Operation::True => true,
            Operation::False => false,
            Operation::And(l) => l.iter().all(|v| v.apply(data, side_effect)),
            Operation::Or(l) => l.iter().any(|v| v.apply(data, side_effect)),
            Operation::Any { key, op } => {
                let v = &data[key];
                match v {
                    serde_json::Value::Array(arr) => arr.iter().any(|v| op.apply(v, side_effect)),
                    _ => false,
                }
            }
            Operation::All { key, op } => {
                let v = &data[key];
                match v {
                    serde_json::Value::Array(arr) => arr.iter().all(|v| op.apply(v, side_effect)),
                    _ => false,
                }
            }
            Operation::Export {
                key,
                name,
                when,
                op,
            } => {
                let v = match key {
                    Some(keyname) => &data[keyname],
                    None => &data,
                };

                let status = match op {
                    Some(op) => op.apply(data, side_effect),
                    None => true,
                };

                if match when {
                    None => status,
                    Some(When::Success) => status,
                    Some(When::Failure) => !status,
                    Some(When::Always) => true,
                } {
                    if !v.is_null() {
                        // If the value is a string, v.to_string() adds extra quotes
                        // around it. Let's not do that.
                        match v.as_str() {
                            Some(s) => side_effect.add_env(name, String::from(s)),
                            None => side_effect.add_env(name, v.to_string()),
                        };
                    }
                };
                status
            }
        }
    }

    pub fn matches(&self, data: &serde_json::Value) -> Option<SideEffect> {
        let mut side_effect = SideEffect::new();
        if self.apply(data, &mut side_effect) {
            Some(side_effect)
        } else {
            None
        }
    }
}

impl SideEffect {
    fn new() -> SideEffect {
        Self {
            env: HashMap::new(),
        }
    }

    fn add_env(&mut self, name: &String, value: String) {
        self.env.insert(name.clone(), value);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json;

    macro_rules! operation_tests {
        ($($name:ident: $value:expr,)*) => {
            $(
                #[test]
                fn $name() {
                    let (condition, result) = $value;
                    let yaml = r#"
                    {
                        "name": "John Doe",
                        "multi": {
                            "nested": {
                                "item": "foo"
                            }
                        },
                        "stringlist": ["baz", "bar", "bat"]
                    }"#;
                    let json : serde_json::Value = serde_json::from_str(yaml).unwrap();
                    if result {
                        assert!(condition.matches(&json).is_some());
                    } else {
                        assert!(condition.matches(&json).is_none());
                    }
                }
            )*
        }
    }

    operation_tests! {
        default_ok: (Operation::True,
            true),
        equals_ok: (Operation::Equals {
                key: Some(String::from("name")),
                value: String::from("John Doe")
            },
            true),
        equals_nok: (Operation::Equals {
                key: Some(String::from("name")),
                value: String::from("JohnDoe")
            },
            false),
        equals_bad_key_nok: (Operation::Equals {
                key: Some(String::from("name2")),
                value: String::from("John Doe"),
            },
            false),
        substring_ok: (Operation::Substring {
                key: Some(String::from("name")),
                needle: String::from("hn D"),
            },
            true),
        substring_nok: (Operation::Substring {
                key: Some(String::from("name")),
                needle: String::from("hnD"),
            },
            false),
        substring_bad_key_nok: (Operation::Substring {
                key: Some(String::from("_name")),
                needle: String::from("hn D"),
            },
            false),
        follow_1_ok: (Operation::Follow {
                key: String::from("multi"),
                op: Box::new(Operation::True),
            },
            true),
        follow_1_nok: (Operation::Follow {
                key: String::from("nulti"),
                op: Box::new(Operation::True),
            },
            false),
        follow_2_ok: (Operation::Follow {
                key: String::from("multi"),
                op: Box::new(Operation::Follow {
                    key: String::from("nested"),
                    op: Box::new(Operation::True),
                }),
            },
            true),
        follow_2_nok: (Operation::Follow {
                key: String::from("multi"),
                op: Box::new(Operation::Follow {
                    key: String::from("pested"),
                    op: Box::new(Operation::True),
                }),
            },
            false),
        follow_then_equals_item_directly_ok: (Operation::Follow {
                key: String::from("multi"),
                op: Box::new(Operation::Follow {
                    key: String::from("nested"),
                    op: Box::new(Operation::Follow {
                        key: String::from("item"),
                        op: Box::new(Operation::Equals {
                            key: None, // we're inside "item", so we don't have keys anymore
                            value: String::from("foo"), // should match
                        }),
                    }),
                }),
            },
            true),
        follow_then_equals_item_directly_nok: (Operation::Follow {
                key: String::from("multi"),
                op: Box::new(Operation::Follow {
                    key: String::from("nested"),
                    op: Box::new(Operation::Follow {
                        key: String::from("item"),
                        op: Box::new(Operation::Equals {
                            key: None, // we're inside "item", so we don't have keys anymore
                            value: String::from("bar"),  // does not match
                        }),
                    }),
                }),
            },
            false),
        follow_then_equals_item_directly_with_key_nok: (Operation::Follow {
                key: String::from("multi"),
                op: Box::new(Operation::Follow {
                    key: String::from("nested"),
                    op: Box::new(Operation::Follow {
                        key: String::from("item"),
                        op: Box::new(Operation::Equals {
                            // invalid, we have no keys here
                            key: Some(String::from("foo")),
                            value: String::from("foo"),
                        }),
                    }),
                }),
            },
            false),
        follow_then_substring_item_directly_ok: (Operation::Follow {
                key: String::from("multi"),
                op: Box::new(Operation::Follow {
                    key: String::from("nested"),
                    op: Box::new(Operation::Follow {
                        key: String::from("item"),
                        op: Box::new(Operation::Substring {
                            key: None, // we're inside "item", so we don't have keys anymore
                            needle: String::from("foo"), // should match
                        }),
                    }),
                }),
            },
            true),
        follow_then_substring_item_directly_nok: (Operation::Follow {
                key: String::from("multi"),
                op: Box::new(Operation::Follow {
                    key: String::from("nested"),
                    op: Box::new(Operation::Follow {
                        key: String::from("item"),
                        op: Box::new(Operation::Substring {
                            key: None, // we're inside "item", so we don't have keys anymore
                            needle: String::from("bar"),  // does not match
                        }),
                    }),
                }),
            },
            false),
        follow_then_substring_item_directly_with_key_nok: (Operation::Follow {
                key: String::from("multi"),
                op: Box::new(Operation::Follow {
                    key: String::from("nested"),
                    op: Box::new(Operation::Follow {
                        key: String::from("item"),
                        op: Box::new(Operation::Substring {
                            // invalid, we have no keys here
                            key: Some(String::from("foo")),
                            needle: String::from("foo"),
                        }),
                    }),
                }),
            },
            false),
        and_1_ok: (Operation::And ([
                Operation::True,
            ].to_vec()),
            true),
        and_1_nok: (Operation::And ([
                Operation::False,
            ].to_vec()),
            false),
        and_2_ok_ok: (Operation::And ([
                Operation::True,
                Operation::True,
            ].to_vec()),
            true),
        and_2_nok_ok: (Operation::And ([
                Operation::False,
                Operation::True,
            ].to_vec()),
            false),
        and_2_ok_nok: (Operation::And ([
                Operation::True,
                Operation::False,
            ].to_vec()),
            false),
        and_2_nok_nok: (Operation::And ([
                Operation::False,
                Operation::False,
            ].to_vec()),
            false),
        or_1_ok: (Operation::Or ([
                Operation::True,
            ].to_vec()),
            true),
        or_1_nok: (Operation::Or ([
                Operation::False,
            ].to_vec()),
            false),
        or_2_ok_ok: (Operation::Or ([
                Operation::True,
                Operation::True,
            ].to_vec()),
            true),
        or_2_nok_ok: (Operation::Or ([
                Operation::False,
                Operation::True,
            ].to_vec()),
            true),
        or_2_ok_nok: (Operation::Or ([
                Operation::True,
                Operation::False,
            ].to_vec()),
            true),
        or_2_nok_nok: (Operation::Or ([
                Operation::False,
                Operation::False,
            ].to_vec()),
            false),
        not_ok: (Operation::Not (Box::new(Operation::False)),
            true),
        not_nok: (Operation::Not (Box::new(Operation::True)),
            false),
        any_list_ok: (Operation::Any {
            key: String::from("stringlist"),
            op: Box::new(Operation::Equals {
                key: None,
                value: String::from("baz"),
            }),
        }, true),
        any_list_nok: (Operation::Any {
            key: String::from("stringlist"),
            op: Box::new(Operation::Equals {
                key: Some(String::from("stringlist")), // invalid key
                value: String::from("baz"),
            }),
        }, false),
        all_list_ok: (Operation::Any {
            key: String::from("stringlist"),
            op: Box::new(Operation::Substring {
                key: None,
                needle: String::from("ba"),
            }),
        }, true),
        all_list_nok: (Operation::Any {
            key: String::from("stringlist"),
            op: Box::new(Operation::Substring {
                key: Some(String::from("stringlist")), // invalid key
                needle: String::from("ba"),
            }),
        }, false),
        export_true_ok: (Operation::Export {
            key: Some(String::from("does-not-matter")),
            name: String::from("unused"),
            when: None,  // When has no effect on the return value
            op: Some(Box::new(Operation::True)),
        }, true),
        export_false_nok: (Operation::Export {
            key: Some(String::from("does-not-matter")),
            name: String::from("unused"),
            when: None,  // When has no effect on the return value
            op: Some(Box::new(Operation::False)),
        }, false),
    }

    macro_rules! export_tests {
        ($($name:ident: $value:expr,)*) => {
            $(
                #[test]
                fn $name() {
                    let (key, value) = $value;
                    let yaml = r#"
                    {
                        "name": "John Doe",
                        "value": 1,
                        "fval": 1.234,
                        "multi": {
                            "nested": {
                                "item": "foo"
                            }
                        },
                        "stringlist": ["baz", "bar", "bat"]
                    }"#;
                    let json : serde_json::Value = serde_json::from_str(yaml).unwrap();

                    for w in vec![None, Some(When::Success), Some(When::Failure), Some(When::Always)].iter() {
                        let condition = Operation::Export {
                            key: Some(String::from(key)),
                            name: String::from("EXPORTED"),
                            when: w.clone(),
                            op: None,
                        };
                        let export = condition.matches(&json).expect("Must not be None");

                        match w {
                            None | Some(When::Success) | Some(When::Always) => {
                                assert_eq!(export.env.get("EXPORTED").unwrap().clone(), String::from(value));
                            },
                            Some(When::Failure) => {
                                assert!(export.env.get("EXPORTED").is_none());
                            },
                        };
                    }
                }
            )*
        }
    }

    export_tests! {
        export_string: ("name", "John Doe"),
        export_int: ("value", "1"),
        export_float: ("fval", "1.234"),
        export_object: ("multi", "{\"nested\":{\"item\":\"foo\"}}"),
        export_array: ("stringlist", "[\"baz\",\"bar\",\"bat\"]"),
    }

    #[test]
    fn export_invalid_key() {
        let yaml = r#"
        {
            "name": "John Doe"
        }"#;
        let json: serde_json::Value = serde_json::from_str(yaml).unwrap();
        let condition = Operation::Export {
            key: Some(String::from("does-not-exist")),
            name: String::from("EXPORT"),
            when: None,
            op: None,
        };
        let export = condition.matches(&json).expect("Must not be None");
        assert!(export.env.get("EXPORT").is_none());
    }
}
