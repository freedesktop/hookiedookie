// SPDX-License-Identifier: MIT License
mod rule;
mod server;
mod settings;

use clap::Parser;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// Validate the given settings file and exit with a nonzero status code if invalid
    #[arg(long)]
    validate: bool,
    settings_file: Option<String>,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let args = Args::parse();

    let settings = args.settings_file.unwrap_or(String::from("Settings.yaml"));

    if args.validate {
        match settings::Settings::from_file(settings.as_str()) {
            Ok(_) => Ok(()),
            Err(err) => {
                println!("Failed to parse settings file: {err}");
                Err(std::io::Error::new(std::io::ErrorKind::InvalidInput, err))
            }
        }
    } else {
        server::run(settings).await
    }
}
